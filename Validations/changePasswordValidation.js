const Joi = require("joi");

module.exports.validations = {
    headers: Joi.object({
        'authorization': Joi.string().required()
        }).options({ allowUnknown: true }),
        payload: Joi.object({
            current_password: Joi.string().min(6).required(),
            new_password:Joi.string().min(6).required()
        }),
        failAction:(request, h, error)=>{
            console.log(error);
            if(error.isJoi ){
            return  h.response(error.details).takeover();
        }
    }
};