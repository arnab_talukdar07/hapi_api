const Joi = require("joi");

module.exports.validations =  {
    payload: Joi.object({
    email: Joi.string().min(3).required().email(),
    password: Joi.string().min(6).required(),
}),
failAction:(request, h, error)=>{
    console.log(error);
    if(error.isJoi ){
    return  h.response(error.details).takeover();
    }
}};