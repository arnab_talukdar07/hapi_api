const controller= require('../Controller/index');
const myPostValidation = require('../Validations/myPostValidation')
const createPostValidation = require('../Validations/createPostValidation');
const editPostValidation = require('../Validations/editPostValidation')
module.exports.apiRoutes=
    [
        {
            method:'POST',
            path:'/post/createPost',
            config:{
                validate: createPostValidation.validations,
                handler:controller.createPost,
                tags: ['api'],
                description:"This api is used to create Posts."
            }
    
        },
        {
            method:'GET',
            path:'/post/allPosts',
            config:{
                handler:controller.allPosts,
                tags: ['api'],
                description:"This api is used to get all the posts."
            }
        },
        {
            method:'POST',
            path:'/post/myPost',
            config:{
                validate:myPostValidation.validations,
                handler:controller.myPost,
                tags: ['api'],
                description:"This api is used to get posts of a single user."
            }
    
        },
        {
            method:'POST',
            path:'/post/editPost',
            config:{
                validate:editPostValidation.validations,
                handler:controller.editPost,
                tags: ['api'],
                description:"This api is used to get posts of a single user."
            }
    
        }
    ]
