'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  console.log("om")
  return db.createTable('users',{
    "user_id":{type:'string',  primaryKey: true  },  
    "firstName": "string",
    "lastName": "string",
    "phone_number": "string",
    "email": "string",
    "password": {type:'string',length: 100},
    "friends":{type:'string', array:true},
    "friend_requests":{type:'string', array:true},
    "sent_requests":{type:'string', array:true},
  }).then(()=>{
    return db.createTable('posts',{
      post_id:{type:'int',  primaryKey: true  , autoIncrement:true},
      author:'string',
      content: 'string',
      previewText:'string',
      thumbnail:{type:'string',length: 400},
      title:'string',
      updatedDate:'string'
    })
    })
  // })
  
  }


exports.down = function(db) {
  console.log("om2")
  return db.dropTable('users').then(()=>{
    db.dropTable('posts');
  });
};

exports._meta = {
  "version": 1
};
