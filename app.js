// 'use strict';
const Hapi=require("@hapi/hapi");
const Inert = require('@hapi/inert');
const Vision = require('@hapi/vision');
const HapiSwagger = require('hapi-swagger');
const apiRoutes=require('./routes/index')
 let logo = "████████╗██╗  ██╗███████╗██████╗  ██████╗  ██████╗ \n" +
        "╚══██╔══╝██║  ██║██╔════╝╚════██╗██╔════╝ ██╔═████╗\n" +
        "   ██║   ███████║█████╗   █████╔╝███████╗ ██║██╔██║\n" +
        "   ██║   ██╔══██║██╔══╝   ╚═══██╗██╔═══██╗████╔╝██║\n" +
        "   ██║   ██║  ██║███████╗██████╔╝╚██████╔╝╚██████╔╝\n" +
        "   ╚═╝   ╚═╝  ╚═╝╚══════╝╚═════╝  ╚═════╝  ╚═════╝ "
const init=async ()=>{
    const server=Hapi.Server({
        host:'localhost',
        port:6500
    });
    const swaggerOptions = {
        info: {
                title: 'Test API Documentation',
                version: "1.0.0.0",
            },
        };
    await server.register([{
        plugin: require('hapi-geo-locate'),
        options:{
            enableByDefault: true
        }
    },
    Inert,
    Vision,
    {
        plugin: HapiSwagger,
        options: swaggerOptions
    }
]);

    server.route(apiRoutes.apiRoutes);
    await server.start();
    console.log(logo);
    console.log("Server Started");
}
process.on('unhandledRejection',(err)=>{
    console.log(err);
    process.exit(1);
})
init();