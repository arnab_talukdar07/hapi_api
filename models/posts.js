const connection=require('../dbconfig');

const {DataTypes}=require('sequelize');

const dbConnection= connection.connect;

const Posts=dbConnection.define('posts',{
    post_id:{
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
    },
    author:{
        type: DataTypes.STRING,
        allowNull: false,
    },
    content:{
        type: DataTypes.STRING,
        allowNull: false,   
    },
    previewText:{
        type: DataTypes.STRING,
        allowNull: false,   
    },
    thumbnail:{
        type: DataTypes.STRING,
        allowNull: false,   
    },
    title:{
        type: DataTypes.STRING,
        allowNull: false,   
    },

    updatedDate:{
        type: DataTypes.STRING,
    },

},
{
    freezeTableName: true,
    timestamps: false
});

module.exports.createPosts= function(post){
    console.log(post);
    const poste=Posts.create(post).then((data)=>{
        console.log(data);
    });
    console.log(poste);
    return ({
        post:poste,
        message:"Post Created"
    });
}
module.exports.allPosts= async function(){
    const posts=await Posts.findAll({raw:true});
    return posts;
}
module.exports.myPosts= async function(user_id){
    const posts=await Posts.findAll({raw:true,where: {
        author: user_id
      }});
    return posts;
}
module.exports.editPost=async function(post,post_id){
    const poste=await Posts.update(post,{where : { post_id: post_id }});
    return poste;
}