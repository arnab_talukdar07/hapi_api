const User=require('../../models/users');
const boom = require('@hapi/boom');
const helper = require('../../Helper/index');
const bcrypt = require('bcrypt');  
module.exports={
    getUsers:async(request,h)=>{
        const user=helper.decodeToken(request.headers['authorization']);
        if(user)
        {
            const users= await User.allUser();
            console.log(users);
            for(let i of users){
                console.log(i.username);
            }
            return h.response(users);
        }
    throw boom.badRequest('Not authorized!');
    
    
},
    register :async (request,h)=>{
        console.log(request.payload.password);
        const pass=await bcrypt.hash(request.payload.password,10);
        console.log(pass);
        const user= await User.oneUser(request.payload.email);
        const userPhone = await User.phoneValidation(request.payload.phone_number);
        
    if(!user && !userPhone)
    {
        User.createUser({
            "firstName": request.payload.firstName,
            "lastName": request.payload.lastName,
            "phone_number": request.payload.phone_number,
            "email": request.payload.email,
            "password": pass
          });
     return h.response({message:"User Created"}).code(201);
    }
    else if(userPhone){
        throw boom.badRequest('Phone Number already exists!');
    }
    else{
        throw boom.badRequest('Email already exists!');
    }
    
    
    
    },
    error:(request,h)=>{
        throw boom.notFound('Page not found');
    },
    login:async(request,h)=>{
        console.log(request.payload.password);
        const user= await User.oneUser(request.payload.email);
        const pass=await bcrypt.hash(request.payload.password,10);
        console.log(bcrypt.compare(user.password,pass));
        console.log(user.password);
        bcrypt.compare(pass, user.password, function(err, result) {
            // console.log(err);
            console.log(result);
       });
        if(user!=undefined)
        {
            if(await bcrypt.compare(request.payload.password,user.password)){
               return h.response({message:"Logged in!", token:helper.encodeToken({email:user.email})});
            }
            else{
                throw boom.notFound('Password missmatch!');
            }
        }
        throw boom.notFound('User not found');
    },
    userInfo:async (request,h)=>{
        const user=helper.decodeToken(request.headers['authorization']);
        if(user){
        console.log(user.email);
        const userInfo= await User.oneUser(user.user.email);
        console.log(userInfo);
        userInfo.password="###############";
        return h.response(userInfo);
        }
        throw boom.badRequest('Not authorized!');

    },
    changePassword:async(request,h)=>{
        const user=helper.decodeToken(request.headers['authorization']);
        console.log(user);
        const pass=await bcrypt.hash(request.payload.new_password,10);
        if(user){
            const userInfo= await User.oneUser(user.user.email);
            console.log(userInfo);
            if(await bcrypt.compare(request.payload.current_password,userInfo.dataValues.password)){
               
            User.changePassword(userInfo.dataValues.user_id,pass).then((data)=>{
                console.log(data)
                return h.response(data);
            }).catch(err=>{console.log(err)})
            return h.response({message:"Password Changed"});
        }
        else{
            throw boom.badRequest('Wrong Password');
        }
        
    }
            throw boom.badRequest('Not authorized!');
             // console.log(user)
        
    },
    search:async(request, h)=>{
        const user=helper.decodeToken(request.headers['authorization']);
        console.log(user);
        if(user){
           let users= await User.searchByName(request.payload.searchTerm);
           return h.response(users);
        }
        throw boom.badRequest('Not authorized!');
    },
    sendRequest:async(request,h)=>{
        const today=new Date(Date.now());
        date=today.toDateString().substring(4,15);
        let user=await User.sendRequest(request.payload.user_id,request.payload.sending_id,date);
        return h.response(user);
    }
}