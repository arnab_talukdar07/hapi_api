const connection=require('../dbconfig');
const {DataTypes, QueryTypes}=require('sequelize');

const dbConnection= connection.connect;

const Users=dbConnection.define('users',{
    user_id:{
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
    },
    firstName:{
        type: DataTypes.STRING,
        allowNull: false
    },
    lastName:{
        type: DataTypes.STRING,
        allowNull: false
    },
    email:{
        type: DataTypes.STRING,
        allowNull: false
    },
    phone_number:{
        type: DataTypes.STRING,
        allowNull: false
    },
    password:{
        type: DataTypes.STRING,
        allowNull: false
    },
    friends:{
        type: DataTypes.STRING,
    },
    friend_requests:{
        type: DataTypes.STRING,
    },
    sent_requests:{
        type: DataTypes.STRING,
    },

},
{
    freezeTableName: true,
    timestamps: false
});

module.exports.createUser = function(body){
    Users.create(body).then((data)=>{
        console.log(data.toJSON());
    }).catch(err=>console.log(err))
}
module.exports.allUser= async function(){
    const users=await Users.findAll({raw:true});
    return users;
    // .then((data)=>{
    //     console.log("data :"+data);
    //     return data;
    // })
    // .catch(err=> {return err;})
}
module.exports.oneUser=async function(email){
    const user=await Users.findOne({where : { email: email }});
    console.log(user);
    return user;
}
module.exports.phoneValidation=async function(phone_number){
    const user=await Users.findOne({where : { phone_number: phone_number }},{raw:true});
    console.log(user);
    return user;
}
module.exports.searchByName=async function(term){
    console.log(term);
    try{
    //     const user=await Users.findAll({where :{
    //         firstName: {
    //             $like: '%' + term + '%'
    //         }
    // }},
    // {raw:true});
    const userF = await dbConnection.query("SELECT `user_id`,`firstName`,`lastName`,`email` FROM `users` WHERE `firstName` LIKE '%"+term+"%'", { type: QueryTypes.SELECT });
    const userL = await dbConnection.query("SELECT `user_id`,`firstName`,`lastName`,`email` FROM `users` WHERE `lastName` LIKE '%"+term+"%'", { type: QueryTypes.SELECT });
    let user = userF.concat(userL);
    console.log(user);
    return user;
    }
    catch(err){
        console.log(err);
    }
//     DataTypes.query('SELECT * FROM users WHERE firstName LIKE %'+term+'%'
// ).then(function(projects) {
//   console.log(projects);
//   return projects;
// })
    
}
module.exports.changePassword=async function(user_id,password){
    const user=await Users.update({password:password},{where : { user_id: user_id }});
    console.log(user);
    return user;
};
module.exports.sendRequest=async function(user_id,sendingId,date){
    const user=await Users.findOne({where : { user_id: user_id }});
    const user2=await Users.findOne({where : { user_id: sendingId }});
    let req=JSON.parse(user.sent_requests);
    console.log("hhh"+req[0])
    let req2=JSON.parse(user2.friend_requests);
    if(req==1){
        req=[];
    }
    if(req2==1){
        req2=[];
    }
    // const nuser=await Users.update({sent_requests:JSON.stringify([{
    //     sendingId:sendingId,
    //     sendingDate:date,
    //     accepted:false

    // }])},{where : { user_id: user_id }});
    // const suser=await Users.update({friend_requests:JSON.stringify([{
    //     senderId:user_id,
    //     sendingDate:date,
    //     accepted:false

    // }])},{where : { user_id: sendingId }});
    // console.log(suser);
    return user;
};



