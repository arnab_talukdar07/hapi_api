const Joi = require("joi");

module.exports.validations = {
    headers: Joi.object({
        'authorization': Joi.string().required()
        }).options({ allowUnknown: true }),
        payload: Joi.object({
            post_id:Joi.number().required(),
            Content:Joi.string().min(6).allow(''),
            previewText:Joi.string().min(3).allow(''),
            title:Joi.string().min(3).allow(''),
            thumbnail:Joi.string().min(3).allow(''),
        }),
        failAction:(request, h, error)=>{
            console.log(error);
            if(error.isJoi ){
            return  h.response(error.details).takeover();
        }
    }
};