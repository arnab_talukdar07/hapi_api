const Joi = require("joi");

module.exports.validations = {
    headers: Joi.object({
        'authorization': Joi.string().required()
        }).options({ allowUnknown: true }),
        payload: Joi.object({
            Content:Joi.string().min(6).required(),
            previewText:Joi.string().min(3).required(),
            title:Joi.string().min(3).required(),
            thumbnail:Joi.string().min(3).required(),
        }),
        failAction:(request, h, error)=>{
            console.log(error);
            if(error.isJoi ){
            return  h.response(error.details).takeover();
        }
    }
};