const Joi = require("joi");

module.exports.validations = {
    headers: Joi.object({
        'authorization': Joi.string().required()
        }).options({ allowUnknown: true }),
        failAction:(request, h, error)=>{
            console.log(error);
            if(error.isJoi ){
            return  h.response(error.details).takeover();
        }
    }
};