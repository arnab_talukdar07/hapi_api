require('dotenv').config()
const Sequelize=require('sequelize');
const sequelize=new Sequelize('hapi_tutorial',process.env.DB_USER,process.env.DB_PASS,{
    host:process.env.DB_HOST,
    port:process.env.DB_PORT,
    dialect:'mysql'
});

module.exports.connect=sequelize;

// module.exports.getUser=async function(){
//     try{
//         await sequelize.authenticate();
//         console.log("Connected");
//         const [results,metadata]= await sequelize.query('SELECT * FROM users');
//         return results;
       
//     }
//     catch{
//         console.log("Cannot Connect")
//     }
// }



// testConnection();

// sequelize.authenticate().then(()=>{
//     console.log('Connected to the database');

// }).catch(()=>{
//     console.log("could not");
// })