const Joi = require("joi");

module.exports.validations = {
    payload: Joi.object({
    firstName: Joi.string().min(3).required(),
    lastName: Joi.string().min(3).required(),
    phone_number: Joi.string().min(10).max(10).required(),
    email: Joi.string().min(3).required().email(),
    password: Joi.string().min(6).required(),
}),
failAction:(request, h, error)=>{
    console.log(error);
    if(error.isJoi ){
    return  h.response(error.details).takeover();
    }
}
};