const boom = require('@hapi/boom');
const user = require('./user/index')
const post = require('./post/index')
module.exports={
    error:(request,h)=>{
        throw boom.notFound('Page not found');
    },
    getUsers:user.getUsers,
    register:user.register,
    login:user.login,
    userInfo:user.userInfo,
    search:user.search,
    changePassword:user.changePassword,
    sendRequest:user.sendRequest,
    createPost:post.createPost,
    myPost:post.myPost,
    allPosts: post.allPosts,
    editPost:post.editPost
}