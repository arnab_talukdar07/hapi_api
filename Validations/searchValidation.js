const Joi = require("joi");

module.exports.validations = {
    headers: Joi.object({
        'authorization': Joi.string().required()
        }).options({ allowUnknown: true }),
        payload: Joi.object({
            searchTerm:Joi.string().min(3).allow(''),
        }),
        failAction:(request, h, error)=>{
            console.log(error);
            if(error.isJoi ){
            return  h.response(error.details).takeover();
        }
    }
};