const controller= require('../Controller/index');
const getUserValidation = require('../Validations/getUserValidations');
const allUserValidation = require('../Validations/allUserValidation');
const registerValidation = require('../Validations/registerValidation');
const loginValidation = require('../Validations/loginValidation');
const changePasswordValidation = require('../Validations/changePasswordValidation');
const searchValidation = require('../Validations/searchValidation');
const sendRequestValidation = require('../Validations/sendRequestValidations');
const { sendRequest } = require('../models/users');

module.exports.apiRoutes=
    [
        {
            method:'GET',
            path:'/allUsers',
            config:{
                validate : allUserValidation.validations,
                handler:controller.getUsers,
                tags: ['api'],
                description:"This api is used to get all the users."
            }
        },
        {
            method:'GET',
            path:'/auth/getUser',
            config:{
                validate : getUserValidation.validations,
                handler : controller.userInfo,
                tags: ['api'],
                description:"This api is used to get all the users."
            }
            
        },
        {
            method:'POST',
            path:'/auth/register',
            config:{
                validate: registerValidation.validations,
                handler:controller.register,
                tags: ['api'],
                description:"This api is used to Register new users."
            }
        },
        {
            method:'POST',
            path:'/auth/login',
            config:{
                validate:loginValidation.validations,
                handler:controller.login,
                tags: ['api'],
                description:"This api is used to Log in users."
            }
        },
        {
            method:'POST',
            path:'/auth/changePassword',
            config:{
                validate: changePasswordValidation.validations,
                handler:controller.changePassword,
                tags: ['api'],
                description:"This api is used to change password of a user."
            }
        },
        {
            method:'POST',
            path:'/auth/search',
            config:{
                validate: searchValidation.validations,
                handler:controller.search,
                tags: ['api'],
                description:"This api is used to Search of a user."
            }
        },
        {
            method:'POST',
            path:'/auth/sendRequest',
            config:{
                validate: sendRequestValidation.validations,
                handler:controller.sendRequest,
                tags: ['api'],
                description:"This api is used to Search of a user."
            }
        },
    ]
