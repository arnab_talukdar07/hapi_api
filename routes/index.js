const controller= require('../Controller/index');
const user = require('./users');
const post = require('./posts');
let apis=user.apiRoutes.concat(post.apiRoutes);
apis=apis.concat([
    {
        method:'GET',
        path:'/{any*}',
        handler:controller.error
    }
])
module.exports.apiRoutes=apis;
    