const boom = require('@hapi/boom');
const Post=require('../../models/posts');
const helper = require('../../Helper/index');
module.exports={
    createPost:(request,h)=>{
        const user=helper.decodeToken(request.headers['authorization']);
        console.log(user);
        if(user){
            console.log(Date.now());
            const today=new Date(Date.now());
            console.log(today.toDateString());
            const post=Post.createPosts({
                "author":user.user.email,
                "content": request.payload.Content,
                "previewText": request.payload.previewText,
                "title": request.payload.title,
                "thumbnail": request.payload.thumbnail,
                "updatedDate":today.toDateString().substring(4,15)
            });
            return h.response(post);
        }
        throw boom.badRequest('Not authorized!');
        // const user=helper.decodeToken(request.payload.token);
        
    },
    myPost:async(request,h)=>{
        const user=helper.decodeToken(request.headers['authorization']);
        console.log(user)
        if(user){
            const posts= await Post.myPosts(user.user.email);
            // console.log(users);
            // for(let i of users){
            //     console.log(i.username);
            // }
            return h.response(posts);
        }
        throw boom.badRequest('Not authorized!');
    },
    allPosts: async (request,h)=>{
        const posts=await Post.allPosts();
        return h.response(posts);
    },
    editPost:async(request ,h)=>{
        const user=helper.decodeToken(request.headers['authorization']);
        console.log(user);
        if(user){
            console.log(Date.now());
            const today=new Date(Date.now());
            console.log(today.toDateString());
            const post= await Post.editPost({
                "author":user.user.email,
                "content": request.payload.Content,
                "previewText": request.payload.previewText,
                "title": request.payload.title,
                "thumbnail": request.payload.thumbnail,
                "updatedDate":today.toDateString().substring(4,15)
            },
            request.payload.post_id
            );
            return h.response(post);
        }
        throw boom.badRequest('Not authorized!');
    }
}